<?php
namespace WLN\AuthTokenBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController as RestController;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class AuthController
 * @package WLN\AuthTokenBundle\Controller
 * @author Jakub "Wolen" Tobiasz <jacob.tobiasz@gmail.com>
 * @copyright 2016 Wolen @ https://bitbucket.org/Wolenho/
 */
class AuthController extends RestController
{
    /**
     * @Rest\View()
     * @Rest\Post("/gettoken")
     *
     * @return array
     */
    public function getTokenAction()
    {
        $tm = $this->get('wln.token_manager');

        return array(
            'token' => $tm->generateJWT()
        );
    }
}