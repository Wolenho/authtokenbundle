<?php
namespace Tests\Util;

use WLN\AuthTokenBundle\Util\TokenManager;

class TokenManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testVerifyJWT()
    {
        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $secret = '64832d39c6be0dfb2257253873bc722d4093b61e';
        $tm = new TokenManager($secret, $em);
        $token = $tm->generateJWT();
        $result = $tm->verifyJWT($token);

        $this->assertEquals(true, $result);
    }
}