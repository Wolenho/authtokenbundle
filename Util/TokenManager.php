<?php
namespace WLN\AuthTokenBundle\Util;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Doctrine\ORM\EntityManager;

/**
 * Class TokenManager
 * @package WLN\AuthTokenBundle\Util
 * @author Jakub "Wolen" Tobiasz <jacob.tobiasz@gmail.com>
 * @copyright 2016 Wolen @ https://bitbucket.org/Wolenho/
 */
class TokenManager
{
    /**
     * @var string $secret
     */
    private $secret;

    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * TokenManager constructor.
     * @param string $secret
     * @param EntityManager $em
     */
    public function __construct($secret, EntityManager $em)
    {
        $this->secret = $secret;
        $this->em = $em;
    }

    /**
     * @return string
     */
    public function generateJWT()
    {
        $builder = new Builder();
        $signer = new Sha256();
        $token = $builder->setIssuer('http://project1.dev')
                            ->setAudience('http:///project1.dev')
                            ->setId('123542', true)
                            ->setIssuedAt(time())
                            ->setNotBefore(time())
                            ->setExpiration(time() + 3600)
                            ->set('access_token', 'example1')
                            ->set('refresh_token', 'example2')
                            ->sign($signer, $this->secret)
                            ->getToken();

        return iconv('UTF-8', 'UTF-8//IGNORE', $token);
    }

    /**
     * @param string
     * @return bool
     */
    public function verifyJWT($token)
    {
        $signer = new Sha256();
        $token = (new Parser())->parse((string) $token);
        return $token->verify($signer, $this->secret);
    }

    /**
     * Function creates and returnes a 22 character
     *
     * @return string Random hash
     */
    protected function generateRandomToken()
    {
        return bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
    }

    /**
     * @param string $type access_token|refresh_token
     * @param int $userId
     * @param string $token
     */
    protected function assignTokenToUser($type, $userId, $token)
    {

    }
}