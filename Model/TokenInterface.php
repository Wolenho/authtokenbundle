<?php
namespace WLN\AuthTokenBundle\Model;

/**
 * Interface TokenInterface
 * @package WLN\AuthTokenBundle\Model
 * @author Jakub Tobiasz <wolenho@gmail.com>
 */
interface TokenInterface
{
    /**
     * @return mixed
     */
    public function getToken();

    /**
     * @param string $token
     * @return boolean
     */
    public function setToken($token);
}