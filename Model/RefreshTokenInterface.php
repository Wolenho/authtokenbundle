<?php
namespace WLN\AuthTokenBundle\Model;

/**
 * Interface RefreshToken
 * @package WLN\AuthTokenBundle\Model
 * @author Jakub Tobiasz <wolenho@gmail.com>
 */
interface RefreshTokenInterface extends TokenInterface
{
}