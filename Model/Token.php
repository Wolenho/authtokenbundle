<?php
namespace WLN\AuthTokenBundle\Model;

abstract class Token implements TokenInterface
{
    /**
     * @var string
     */
    protected $token;

    /**
     * Set token
     *
     * @param string $token
     * @return Token
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * Return token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}